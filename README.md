# ADC3117 Tests OPIs

EPICS OPIs for testing the ADC3117 FMCs in the lab.  



## Conventions
The following directories exist for house-keeping reasons:  
10-Top  
99-Shared  
The '10-Top' directory is intended only for OPIs that can be directly opened by users. These OPIs are often 'laucher' type displays that instantiate template OPIs for a particular system.  
'99-Shared' directory is for objects that can be called/embedded from elsewhere. It generally does not make sense to open these objects directory in the OPI runtime. 
